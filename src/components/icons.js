import React from 'react';

// Icon
const IconStar = function IconStar(props) {
    const starfill = props.starfill || '#010101'

    return (
        <svg className="star" xmlns="http://www.w3.org/2000/svg" width="10" height="10" aria-labelledby="title">
            <title id="title">Star</title>
            <path fill={starfill} d="M6.918 6.133L10 3.75H6.25L5 0 3.75 3.75H0l3.086 2.373L1.875 10l3.13-2.4L8.128 10z"/>
        </svg>
    )
};

//export default IconStar;