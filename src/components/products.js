import React, { Component } from 'react';
import ProductCard from './productCard';

class Products extends Component {
    render() {
        let productItems;
        if(this.props.products) {
            productItems = this.props.products.map(product => {
                return (
                    <ProductCard key={product} product={product} />
                );
            });
        }
        return (
            <div className="App">
                {productItems}
            </div>
        );
    }
}

export default Products;