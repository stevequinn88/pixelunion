import React, { Component } from 'react';
//import IconStar from './icons'; <IconStar />
import Button from '@material-ui/core/Button';
import StarRatingComponent from './starComponent';
import CrossfadeImage from './crossfadeImage';

class ProductCard extends Component {
    changeImage(imageColor) {
        const product = this.props.product;
        const navy = imageColor === 'navy',
            grey = imageColor === 'grey',
            orange = imageColor === 'orange';

        if(navy){
            console.log('NAVY');
            product.imageIndex = 0 ;
            this.setState({ imageIndex: 0 });
        } else if(grey){
            console.log('GREY');
            product.imageIndex = 1 ;
            this.setState({ imageIndex: 1 });
        } else if(orange){
            console.log('ORANGE');
            product.imageIndex = 2 ;
            this.setState({ imageIndex: 2 });
        } else {}
    }

    render() {
        const images = this.props.product.images;
        return (
            <div>
                <ul className="list">
                    <li className="list__item product__card">
                        <Button variant="raised" className="cart__button"
                                color="primary" style={{fontSize: '14px', textTransform: 'capitalize'}}>
                            Add to cart
                        </Button>
                        <CrossfadeImage
                            src={images[this.props.product.imageIndex]}
                            duration={1000}
                            timingFunction={"ease-out"}
                            alt={'true'}
                        />
                        <ul className="list squares">
                            <li onClick={this.changeImage.bind(this, 'navy')} className="list__item square square--navy"> </li>
                            <li onClick={this.changeImage.bind(this, 'grey')} className="list__item square square--grey"> </li>
                            <li onClick={this.changeImage.bind(this, 'orange')} className="list__item square square--orange"> </li>
                        </ul>
                        <div className="soldout__badge"><p className="soldout__badge__text">Sold Out</p> </div>
                        <div className="product__card__text">
                            <p>{this.props.product.title}</p>
                            <p>$ {this.props.product.price}</p>
                        </div>

                        <div className="product__card__stars">
                            <StarRatingComponent rating={this.props.product.rating}/>
                        </div>
                    </li>
                </ul>
            </div>
        );
    }
}

export default ProductCard;


