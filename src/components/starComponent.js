import React, { Component } from 'react';
import StarRatingComponent from 'react-star-rating-component';
//import IconStar from './icons';

class starComponent extends Component {
    constructor(){
        super();
        this.state = {
            rating: 4,
        }
    }
    render() {
        const { rating } = this.state;
        if(this.state.rating) {
            return (
                <div>
                    <StarRatingComponent
                        name="rate2"
                        editing={false}
                        //We could potentially render a custom icon here.
                        //renderStarIcon={() => <span><IconStar /></span>}
                        starCount={5}
                        value={rating}
                        starColor={'#010101'}
                        emptyStarColor={'#DDDDDD'}
                    />
                </div>
            );
        }
    }
}

export default starComponent;