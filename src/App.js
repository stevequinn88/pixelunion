import React, { Component } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Products from './components/products';
import './App.css';
import shirt from './assets/shirt-1.jpg';
import shirt2 from './assets/shirt-2.jpg';
import shirt3 from './assets/shirt-3.jpg';

const images = [
    shirt,
    shirt2,
    shirt3
];

class App extends Component {
    constructor(props){
        super(props);
        this.state = {
            products: []
        }
    }
    componentWillMount() {
        this.setState({
            products: [
                {
                    title: 'Ultrafine Merino T-Shirt',
                    price: 80.00,
                    rating: 4,
                    inStock: false,
                    color: 'navy',
                    images: images,
                    imageIndex: 0
                }
            ],
        });
    }
    render(){
        return (
            <React.Fragment>
                <CssBaseline />
                <div className="App">
                    <Products products={this.state.products} />
                </div>
            </React.Fragment>
        );
    }
}



export default App;
